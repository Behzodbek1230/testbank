import React from "react";
import { Link } from "react-router-dom";
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import { Collapse, CardHeader, CardBody, Card } from "reactstrap";
import classnames from "classnames";
import Region from "../../features/regions/Region.js";
import District from "../../features/districts/Districts.js";
import Home from "../../features/home/index";
class RegionAndDistrict extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "1",
      activePill: "1",
      collapse: [
        { id: 1, collapse: true },
        { id: 2, collapse: false },
        { id: 3, collapse: false },
        { id: 4, collapse: false },
        { id: 5, collapse: false },
        { id: 6, collapse: false },
        { id: 7, collapse: false },
      ],
    };
    this.toggleCollapse = this.toggleCollapse.bind(this);
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  togglePill(pill) {
    if (this.state.activePill !== pill) {
      this.setState({
        activePill: pill,
      });
    }
  }

  toggleCollapse(index) {
    var newArray = [];
    for (let collapseObj of this.state.collapse) {
      if (collapseObj.id === index) {
        collapseObj.collapse = !collapseObj.collapse;
      } else {
        collapseObj.collapse = false;
      }
      newArray.push(collapseObj);
    }

    this.setState({
      collapse: newArray,
    });
  }

  render() {
    return (
      <div>
        <Home />
        <ol className="breadcrumb float-xl-right">
          <li className="breadcrumb-item">
            <Link to="/">Home</Link>
          </li>
          <li className="breadcrumb-item active">Regions and Districts</li>
        </ol>
        <h1 className="page-header" style={{ marginLeft: "250px" }}>
          Regions and Districts
        </h1>
        <div
          className="row"
          style={{ marginLeft: "250px", marginRight: "50px" }}
        >
          <div className="col-xl-12">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "1",
                  })}
                  onClick={() => {
                    this.toggleTab("1");
                  }}
                >
                  <span className="d-sm-none">Districts</span>
                  <span className="d-sm-block d-none">Districts</span>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "2",
                  })}
                  onClick={() => {
                    this.toggleTab("2");
                  }}
                >
                  <span className="d-sm-none">Regions</span>
                  <span className="d-sm-block d-none">Regions</span>
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <District />
              </TabPane>
              <TabPane tabId="2">
                <Region />
              </TabPane>
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default RegionAndDistrict;
