import React from "react";
import ReactTable from "react-table";
import { Panel } from "../../components/panel/panel.jsx";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
// import "react-table/react-table.css";

class District extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalDialog: false,
      regions: [
        {
          code: "1706",
          name: "Buxoro viloyati",
        },
        {
          code: "1708",
          name: "Jizzax viloyati",
        },
        {
          code: "1710",
          name: "Qashqadaryo viloyati",
        },
        {
          code: "1712",
          name: "Navoiy viloyati",
        },
        {
          code: "1714",
          name: "Namangan viloyati",
        },
        {
          code: "1718",
          name: "Samarqand viloyati",
        },
        {
          code: "1722",
          name: "Surxandaryo viloyati",
        },
        {
          code: "1724",
          name: "Sirdaryo viloyati",
        },
        {
          code: "1727",
          name: "Toshkent viloyati",
        },
        {
          code: "1730",
          name: "Farg'ona viloyati",
        },
        {
          code: "1733",
          name: "Xorazm viloyati",
        },
        {
          code: "1726",
          name: "Toshkent shahri",
        },
        {
          code: "1735",
          name: "Qoraqalpog'iston Respublikasi",
        },
        {
          code: "1700",
          name: "Test viloyati",
        },
        {
          code: "1702",
          name: "Test Update viloyati",
        },
        {
          code: "1703",
          name: "Andijon viloyati",
        },
      ],
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.notificationDOMRef = React.createRef();

    this.data = this.state.regions;
    this.defaultSorted = [
      {
        id: "row",
        desc: false,
      },
      //   {
      //     id: "code",
      //     desc: true,
      //   },
      //   {
      //     id: "name",
      //     desc: false,
      //   },
    ];
    this.tableColumns = [
      {
        Header: "",
        id: "row",
        accessor: (d) => d.row,
        maxWidth: 50,
        filterable: false,
        Cell: (row) => {
          return <div>{row.index + 1}</div>;
        },
      },
      {
        Header: "Code",
        id: "code",
        accessor: (d) => d.code,
      },
      {
        Header: "Name",
        // accessor: "name",
        id: "name",
        accessor: (d) => d.name,
      },
      {
        Header: "Region",
        // accessor: "name",
        id: "name",
        accessor: (d) => d.name,
      },
    ];
  }
  toggleModal(name) {
    switch (name) {
      case "modalDialog":
        this.setState({ modalDialog: !this.state.modalDialog });
        break;
      default:
        break;
    }
  }
  render() {
    return (
      <div>
        {/* <ReactNotification ref={this.notificationDOMRef} /> */}
        <h1 className="page-header">Districts Table </h1>
        <Panel>
          <div className="row">
            <div className="col-xl-12">
              <button
                onClick={() => this.toggleModal("modalDialog")}
                className="btn btn-success"
              >
                Add new District &nbsp;<i className="fa fa-plus"></i>
              </button>
              <Modal
                isOpen={this.state.modalDialog}
                toggle={() => this.toggleModal("modalDialog")}
              >
                <ModalHeader toggle={() => this.toggleModal("modalDialog")}>
                  Add new District
                </ModalHeader>
                <ModalBody>
                  <p>Modal body content here...</p>
                </ModalBody>
                <ModalFooter>
                  <button
                    className="btn btn-white"
                    onClick={() => this.toggleModal("modalDialog")}
                  >
                    Close
                  </button>
                  <button className="btn btn-success">Save</button>
                </ModalFooter>
              </Modal>
            </div>
          </div>
          <hr></hr>
          <ReactTable
            filterable
            data={this.data}
            columns={this.tableColumns}
            defaultPageSize={10}
            defaultSorted={this.defaultSorted}
            className="-highlight"
          />
        </Panel>
      </div>
    );
  }
}

export default District;
