import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { Styles } from "./styles/account.js";
let url = "http://templ-api.webase.uz/Account/GenerateToken";

function Login() {
  let history = useHistory();
  useEffect(() => {
    const form = document.getElementById("form_login");
    const user = document.getElementById("login_user");
    const password = document.getElementById("login_password");

    form.addEventListener("submit", formSubmit);

    async function formSubmit(e) {
      e.preventDefault();

      const userValue = user.value.trim();
      const passwordValue = password.value.trim();

      if (userValue === "") {
        setError(user, "User can't be blank");
      } else {
        setSuccess(user);
      }

      if (passwordValue === "") {
        setError(password, "Password can't be blank");
      } else {
        setSuccess(password);
      }

      const data = {
        username: userValue,
        password: passwordValue,
      };

      (async () => {
        const rawResponse = await fetch(url, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        });
        const content = await rawResponse.json();

        //   qaysi role bo'lsa o'shani tekshirish
        console.log(content.token);
        if (rawResponse.status == 200) {
          localStorage.setItem("token", content.token);
          history.push("/home");
        }
        if (rawResponse.status === 400) {
          if (content.error) {
            setError(password, content.error);
            setError(user, content.error);
          }
        } else {
          localStorage.setItem("token", content.token);
          history.push("/home");
          if (content.token) {
            localStorage.setItem("token", content.token);
            history.push("/home");
          }
        }
      })();
    }

    function setError(input, message) {
      const formControl = input.parentElement;
      const errorMsg = formControl.querySelector(".login_input-msg");
      formControl.className = "form-control text-left error";
      errorMsg.innerText = message;
    }

    function setSuccess(input) {
      const formControl = input.parentElement;
      formControl.className = "form-control success";
    }
  }, []);

  return (
    <Styles>
      {/* Main Wrapper */}
      <div className="main-wrapper login-page">
        {/* Header 2 */}

        {/* Breadcroumb */}

        {/* Login Area */}
        <section className="login-area">
          <Container>
            <Row>
              <Col md="12">
                <div className="login-box" style={{ backgroundColor: "white" }}>
                  <div className="login-title text-center">
                    <h3>Log In</h3>
                  </div>
                  <form id="form_login" className="form">
                    <p className="form-control">
                      <label htmlFor="login_user">User Name</label>
                      <input
                        type="text"
                        placeholder="Username"
                        id="login_user"
                      />
                      <span className="login_input-msg"></span>
                    </p>
                    <p className="form-control">
                      <label htmlFor="login_password">Password</label>
                      <input
                        type="password"
                        placeholder="*******"
                        id="login_password"
                      />
                      <span className="login_input-msg"></span>
                    </p>
                    <button>Log In</button>
                  </form>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/* Footer 2 */}
      </div>
    </Styles>
  );
}

export default Login;
