import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import MaterialTable from "material-table";
import SearchIcon from "@material-ui/icons/Search";
import IconButton from "@material-ui/core/IconButton";

import axios from "axios";

import Home from "../home";
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "25ch",
  },
}));

function Bank() {
  const classes = useStyles();
  const [inn, setinn] = useState("");
  const [shortname, setshortname] = useState("");
  const [fullname, setfullname] = useState("");
  const [director, setdirector] = useState("");
  const [mobilenumber, setmobilenumber] = useState("");
  const [oblasname, setoblasname] = useState([]);
  const [regis, setregis] = useState("");
  const [regislist, setregislist] = useState([]);
  const [loanrows, setLoanRows] = useState([]);
  const [data, setData] = useState(0);
  const [id, setId] = useState(0);
  const [district, setdistrict] = useState(0);
  const tokenStr = localStorage.getItem("token");
  const [obl, setobl] = useState("");
  const [idob, setidob] = useState(0);
  const [contactinfo, setcontactinfo] = useState("");
  const [adress, setadress] = useState("");
  const [accounter, setaccounter] = useState("");
  const [vatcode, setvatcode] = useState("");
  const [oked, setoked] = useState("");
  const [Accounts, setAccounts] = useState([]);
  // const [role, setrole] = useState([]);

  useEffect(() => {
    axios
      .get(`http://templ-api.webase.uz/Oblast/GetAll`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenStr,
        },
      })
      .then((response) => {
        setoblasname(response.data);
      })
      .catch((error) => console.log(error));
    const role = document.getElementById("registration_regions");
    document.getElementById("registration_regions").onclick = function () {
      axios
        .get(
          `http://templ-api.webase.uz/Region/GetAll?OblastID=${role.value}`,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + tokenStr,
            },
          }
        )
        .then((response) => {
          setregislist(response.data);
        })
        .catch((error) => console.log(error));
    };
  }, []);

  const handleChangesearch = () => {
    axios
      .get(`http://templ-api.webase.uz/Oblast/GetAll`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenStr,
        },
      })
      .then((response) => {
        setoblasname(response.data);
      })
      .catch((error) => console.log(error));

    axios
      .get(
        `http://templ-api.webase.uz/Contractor/Get?inn=${
          document.getElementById("inn").value
        }`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenStr,
          },
        }
      )
      .then((response) => {
        if (response.data) {
          setId(response.data.id);
          setData(response.data);
          setAccounts(response.data.Accounts);
          setidob(response.data.oblastid);
          setshortname(response.data.shortname);
          setfullname(response.data.fullname);
          setdirector(response.data.director);
          setmobilenumber(response.data.mobilenumber);
          setcontactinfo(response.data.contactinfo);
          setadress(response.data.adress);
          setaccounter(response.data.accounter);
          setvatcode(response.data.vatcode);
          setoked(response.data.oked);
          setdistrict(response.data.regionid);

          axios
            .get(
              `http://templ-api.webase.uz/Oblast/Get?id=${response.data.oblastid}`,
              {
                headers: {
                  "Content-Type": "application/json",
                  Authorization: "Bearer " + tokenStr,
                },
              }
            )
            .then((response) => {
              setobl(response.data.shortname);
            });
          axios
            .get(
              `http://templ-api.webase.uz/Region/Get?id=${response.data.regionid}`,
              {
                headers: {
                  "Content-Type": "application/json",
                  Authorization: "Bearer " + tokenStr,
                },
              }
            )
            .then((response) => {
              setregis(response.data);
            })
            .catch((error) => console.log(error));
        }
        setData(response.data.oblastid);
      })

      .catch((error) => {
        setfullname("");
        setdirector("");
        setidob("");
        setmobilenumber("");
        setshortname("");
        setobl("");
        setcontactinfo("");
        setadress("");
        setaccounter("");
        setvatcode("");
        setoked("");
        setAccounts([]);
      });
  };
  const handleSave = () => {
    let inn = document.getElementById("inn").value;
    let shortname = document.getElementById("shortname").value;
    let fullname = document.getElementById("loan-fullname").value;
    let director = document.getElementById("director").value;
    let mobilenumber = document.getElementById("director-mobilenumber").value;
    let contactinfo = document.getElementById("contactinfo").value;
    let adress = document.getElementById("adress").value;
    let accounter = document.getElementById("accounter").value;
    let vatcode = document.getElementById("vatcode").value;
    let oked = document.getElementById("oked").value;
    let oblastid = document.getElementById("registration_regions").value;
    let regionid = document.getElementById("registration_district").value;

    let postdata = {
      id: id,
      shortname: shortname,
      fullname: fullname,
      inn: inn,
      adress: adress,
      vatcode: vatcode,
      contactinfo: contactinfo,
      mobilenumber: mobilenumber,
      oblastid: oblastid,
      regionid: regionid,
      accounter: accounter,
      director: director,
      oked: oked,
      isbudget: true,
      accounts: Accounts,

      branches: [{ branchcode: " ", branchname: "" }],
    };
    axios
      .post(`http://templ-api.webase.uz/Contractor/Update`, postdata, {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",

          Authorization: "Bearer " + tokenStr,
        },
      })
      .then((response) => {
        console.log("post-response", response.data);
      })
      .catch((error) => console.log(error));
  };

  const handleAddLoan = () => {
    let code = [];
    let bankname = [];
    let accountname = [];
    let statename = [];

    // const loandetails = [
    //   {
    //     code: code,
    //     accountname: accountname,
    //     bankname: bankname,
    //     statename: statename,
    //     // fullname: fullname,
    //     // director: director,
    //     // mobilenumber: mobilenumber,
    //     // contactinfo: contactinfo,
    //     // adress: adress,
    //     // accounter: accounter,
    //     // vatcode: vatcode,
    //     // oked: oked,
    //     // oblastid: oblastid,
    //   },
    // ];

    // setLoanRows(loandetails);
    //setLoanProgram(event.target.value);
  };

  const margintype = "normal";
  let columns = [
    { title: "Account code", field: "code" },
    { title: "Account name", field: "accountname" },
    { title: "Bankname", field: "bankname" },

    { title: "Status", field: "statename" },
  ];

  return (
    <div>
      <Home />
      <div
        className="loan-form"
        style={{
          width: "2000px",
          height: "3500px",
          fontSize: "15px",
          marginRight: "100px",
        }}
      >
        <div className="obligations">
          <div className="existing-loans">
            <div
              style={{
                marginRight: "200px",
                marginLeft: "300px",
                marginTop: "50px",
                backgroundColor: "white",
                height: "600px",
              }}
            >
              <TextField
                id="inn"
                placeholder="INN"
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "60px",
                }}
                label="INN"
                value={inn}
                onChange={(e) => setinn(e.target.value)}
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <IconButton
                style={{
                  marginLeft: "-50px",
                  marginTop: "50px",
                }}
                type="submit"
                onClick={handleChangesearch}
                className={classes.iconButton}
                aria-label="search"
              >
                <SearchIcon />
              </IconButton>
              <TextField
                id="shortname"
                value={shortname}
                onChange={(e) => setshortname(e.target.value)}
                style={{
                  width: "600px",
                  marginLeft: "30px",
                  marginTop: "60px",
                }}
                placeholder="Shortname"
                label="Shortname"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <TextField
                id="director-mobilenumber"
                value={mobilenumber}
                onChange={(e) => setmobilenumber(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "100px",
                  marginTop: "60px",
                }}
                placeholder="Mobilenumber"
                label="Mobilenumber"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <br />
              <select
                style={{
                  height: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                id="registration_regions"
              >
                {obl ? (
                  <option value={idob}>{obl}</option>
                ) : (
                  <option>Select Region</option>
                )}
                {oblasname.map((option) => (
                  <option key={option.id} value={option.id}>
                    {option.name}
                  </option>
                ))}
              </select>
              <select
                style={{
                  height: "50px",
                  marginLeft: "60px",
                  marginTop: "50px",
                  width: "350px",
                }}
                id="registration_district"
                // onChange={onchangedis()}
              >
                {obl ? (
                  <option value={district}>{regis.fullname}</option>
                ) : (
                  <option>Select District</option>
                )}
                {regislist.map((option) => (
                  <option key={option.id} value={option.id}>
                    {option.name}
                  </option>
                ))}
              </select>
              <TextField
                id="loan-fullname"
                onChange={(e) => setfullname(e.target.value)}
                style={{
                  width: "600px",
                  marginLeft: "50px",
                  marginTop: "50px",
                }}
                placeholder="Fullname"
                label="Fullname"
                value={fullname}
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <br />

              <TextField
                id="director"
                value={director}
                onChange={(e) => setdirector(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="director"
                label="director"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <TextField
                id="contactinfo"
                value={contactinfo}
                onChange={(e) => setcontactinfo(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="contactinfo"
                label="Contactinfo"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <TextField
                id="adress"
                value={adress}
                onChange={(e) => setadress(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="Adress"
                label="Adress"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <br />
              <TextField
                id="accounter"
                value={accounter}
                onChange={(e) => setaccounter(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="Accounter"
                label="Accounter"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <TextField
                id="vatcode"
                value={vatcode}
                onChange={(e) => setvatcode(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="Vatcode"
                label="Vatcode"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
              <TextField
                id="oked"
                value={oked}
                onChange={(e) => setoked(e.target.value)}
                style={{
                  marginRight: "50px",
                  marginLeft: "50px",
                  marginTop: "50px",
                  width: "350px",
                }}
                placeholder="Oked"
                label="Oked"
                fullWidth
                className={classes.textField}
                margin={margintype}
              />
            </div>

            <div></div>
          </div>
        </div>
        <div
          style={{
            marginLeft: "300px",
            marginRight: "200px",
          }}
        >
          <MaterialTable
            title="Employee Data"
            data={Accounts}
            columns={columns}
            options={{
              paging: false,
            }}
            editable={{
              onRowAdd: (newRow) =>
                new Promise((resolve, reject) => {
                  const updatedRows = [
                    ...Accounts,
                    { id: Math.floor(Math.random() * 100), ...newRow },
                  ];
                  setTimeout(() => {
                    setAccounts(updatedRows);
                    resolve();
                  }, 2000);
                }),
              onRowDelete: (selectedRow) =>
                new Promise((resolve, reject) => {
                  const index = selectedRow.tableData.id;
                  const updatedRows = [...Accounts];
                  updatedRows.splice(index, 1);
                  setTimeout(() => {
                    setAccounts(updatedRows);
                    resolve();
                  }, 2000);
                }),
              onRowUpdate: (updatedRow, oldRow) =>
                new Promise((resolve, reject) => {
                  const index = oldRow.tableData.id;
                  const updatedRows = [...Accounts];
                  updatedRows[index] = updatedRow;
                  setTimeout(() => {
                    setAccounts(updatedRows);
                    resolve();
                  }, 2000);
                }),
            }}
          />
        </div>
        <div
          style={{
            marginLeft: "1730px",
            marginTop: "10px",
            marginBottom: "50px",
          }}
        >
          <Button
            variant="contained"
            color="primary"
            component="span"
            onClick={handleSave}
          >
            Save
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Bank;
