import styled from "styled-components";
import { colors } from "./elements.js";

export const Styles = styled.div`
  .registration-page {
    .registration-area {
      padding: 50px 0;
      .registration-box {
        .registration-title {
          h3 {
            color: ${colors.black2};
            text-transform: uppercase;
            font-weight: 600;
            padding-bottom: 10px;
            margin-bottom: 20px;
            position: relative;
            &:before {
              position: absolute;
              content: "";
              background: ${colors.green};
              width: 50px;
              height: 2px;
              bottom: 0;
              left: 50%;
              margin-left: -25px;
            }

            @media (max-width: 575px) {
              font-size: 20px;
            }
          }
        }
        .image--cover {
          width: 150px;
          height: 150px;
          border-radius: 50%;

          object-fit: cover;
          object-position: center right;
        }
        form.form {
          table {
            width: 100%;
          }
          tr {
            width: 100%;
          }
          td {
            width: 50%;
          }

          p.form-control {
            padding: 10px;
            width: auto;
            height: auto;
            background: transparent;
            border: none;
            margin-bottom: 28px;
            position: relative;

            label {
              font-size: 15px;
              color: ${colors.text1};
              font-weight: 500;

              @media (max-width: 575px) {
                font-size: 14px;
              }
            }

            input {
              width: 100%;
              height: 38px;
              background-color: #ffffff;
              font-size: 14px;
              padding: 15px 20px;
              color: ${colors.black1};
              border: 1px solid ${colors.border3};
              border-radius: 5px;

              &::placeholder {
                font-size: 14px;
                font-style: italic;
                color: ${colors.text3};

                @media (max-width: 575px) {
                  font-size: 13px;
                }
              }

              &:focus {
                border-color: ${colors.green};
              }

              @media (max-width: 575px) {
                height: 40px;
              }
            }

            span {
              color: ${colors.red};
              font-weight: 300;
              position: absolute;
              bottom: -20px;
              left: 0;
              visibility: hidden;
            }
          }
          p.form-controln {
            padding: 10px;
            width: auto;
            height: auto;
            text-align: left;
            background: transparent;
            border: none;
            margin-bottom: 28px;
            position: relative;
            table {
              width: 100%;
            }

            label {
              display: block;
              font-size: 15px;
              color: ${colors.text1};
              font-weight: 500;

              @media (max-width: 575px) {
                font-size: 14px;
              }
            }
            select {
              width: 100%;
              height: 38px;
              background-color: #ffffff;
              font-size: 14px;
              padding: 10px 10px;
              color: "colors.black1";
              border: 1px solid "${colors.border3}";
              border-radius: 5px;
            }

            input {
              width: 100%;
              padding-right: 500px;
              height: 38px;
              background-color: #ffffff;
              font-size: 14px;
              padding: 15px 0px;
              color: ${colors.black1};
              border: 1px solid ${colors.border3};
              border-radius: 5px;

              &::placeholder {
                font-size: 14px;
                font-style: italic;
                color: ${colors.text3};

                @media (max-width: 500px) {
                  font-size: 13px;
                }
              }

              &:focus {
                border-color: ${colors.green};
              }

              @media (max-width: 575px) {
                height: 40px;
              }
            }

            span {
              color: ${colors.red};
              font-weight: 300;
              position: absolute;
              bottom: -20px;
              left: 0;
              visibility: hidden;
            }
          }

          p.form-control.success {
            input {
              border: 2px solid ${colors.green};
            }

            &::before {
              position: absolute;
              content: "\f058";
              font-family: "Line Awesome Free";
              font-size: 24px;
              color: ${colors.green};
              font-weight: 900;
              top: 34px;
              right: 10px;
            }
          }

          p.form-control.error {
            input {
              border: 2px solid ${colors.red};
            }

            &::before {
              position: absolute;
              content: "\f06a";
              font-family: "Line Awesome Free";
              font-size: 24px;
              color: ${colors.red};
              font-weight: 900;
              top: 34px;
              right: 10px;
            }
          }

          p.form-control.error {
            span {
              visibility: visible;
            }
          }

          button {
            font-size: 14px;
            color: #fff;
            background: ${colors.gr_bg};
            width: 100%;
            height: 48px;
            font-weight: 500;
            border: none;
            border-radius: 5px;
            text-transform: uppercase;
            margin-bottom: 20px;
            tr {
              width: 100%;
            }
            td.lef {
              text-align: right;
              width: 80%;
            }

            &:hover {
              background: ${colors.gr_bg2};

              i {
                color: #ffffff;
              }
            }

            @media (max-width: 575px) {
              font-size: 15px;
              height: 40px;
            }
          }
        }

        .have_account-btn {
          p {
            font-size: 14px;
            color: ${colors.text3};
            a {
              font-size: 14px;
              color: ${colors.green};
              font-weight: 500;
              &:hover {
                text-decoration: underline;
              }
            }
          }
        }
      }

      @media (max-width: 767px) {
        padding: 30px 0;
      }
    }
  }
`;
