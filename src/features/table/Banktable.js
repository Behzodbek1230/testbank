import React, { useEffect, useState } from "react";
import MaterialTable from "material-table";
import axios from "axios";
import Home from "../home";
import SearchBar from "material-ui-search-bar";
import { TablePagination } from "@material-ui/core";

let tokenStr = localStorage.getItem("token");

function App() {
  const [res, setRes] = useState([]);
  const [datas, setData] = useState([]);
  const [searched, setSearched] = useState("");
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const handleChangePage = (event, newPage) => {
    console.log(newPage);
    axios
      .get(
        `http://templ-api.webase.uz/Bank/GetList?PageNumber=${newPage}&PageLimit=${rowsPerPage}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenStr,
          },
        }
      )
      .then((response) => {
        console.log(response.data.rows);
        setData(response.data.rows);
      })

      .catch((error) => console.log(error));
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    console.log(parseInt(event.target.value, 10));
    axios
      .get(
        `http://templ-api.webase.uz/Bank/GetList?PageNumber=${page}&PageLimit=${parseInt(
          event.target.value,
          10
        )}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenStr,
          },
        }
      )
      .then((response) => {
        setData(response.data.rows);
      })

      .catch((error) => console.log(error));
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const requestSearch = (searchedVal) => {
    axios
      .get("http://templ-api.webase.uz/Bank/GetList?Search=" + searchedVal, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenStr,
        },
      })
      .then((response) => {
        setData(response.data.rows);
      })

      .catch((error) => console.log(error));
  };

  const cancelSearch = () => {
    setSearched("");
    requestSearch(searched);
  };
  useEffect(() => {
    axios
      .get(
        `http://templ-api.webase.uz/Bank/GetList?PageNumber=${page}&PageLimit=${rowsPerPage}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenStr,
          },
        }
      )
      .then((response) => {
        setData(response.data.rows);
      })

      .catch((error) => console.log(error));
    axios
      .get(`http://templ-api.webase.uz/Bank/GetList`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenStr,
        },
      })
      .then((response) => {
        setRes(response.data.rows);
      })

      .catch((error) => console.log(error));
  }, []);

  let columns = [
    { title: "ID", field: "id" },
    { title: "Code", field: "code" },
    { title: "Bankname", field: "bankname" },
    { title: "status", field: "status" },
  ];

  return (
    <div
      className="App"
      style={{ marginRight: "100px", marginLeft: "250px", marginTop: "100px" }}
    >
      <Home />
      <SearchBar
        value={searched}
        onChange={(searchVal) => requestSearch(searchVal)}
        onCancelSearch={() => cancelSearch()}
      />
      <MaterialTable
        title="Employee Data"
        data={datas}
        columns={columns}
        options={{
          search: false,
          paging: false,
          // filtering: true,
          exportButton: true,
        }}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              const updatedRows = [
                ...datas,
                { id: Math.floor(Math.random() * 100), ...newRow },
              ];
              setTimeout(() => {
                setData(updatedRows);
                resolve();
              }, 2000);
            }),
          onRowDelete: (selectedRow) =>
            new Promise((resolve, reject) => {
              const index = selectedRow.tableData.id;
              const updatedRows = [...datas];
              let indel = updatedRows[index];
              updatedRows.splice(index, 1);
              console.log(indel.id);
              setTimeout(() => {
                (async () => {
                  let response = await fetch(
                    "http://templ-api.webase.uz/Bank/Delete?id=" + indel.id,
                    {
                      method: "DELETE",
                      headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        Authorization: "Bearer " + tokenStr,
                      },
                    }
                  );
                  let result = await response.json();
                  console.log(result);
                })();

                setData(updatedRows);
                resolve();
              }, 2000);
            }),
          onRowUpdate: (updatedRow, oldRow) =>
            new Promise((resolve, reject) => {
              const index = oldRow.tableData.id;
              const updatedRows = [...datas];
              updatedRows[index] = updatedRow;

              setTimeout(() => {
                let dataedit = {
                  id: updatedRows[index].id,
                  code: updatedRows[index].code,
                  bankname: updatedRows[index].bankname,
                  stateid: updatedRows[index].status == "Актив" ? 1 : 0,
                };
                console.log(dataedit);
                (async () => {
                  let response = await fetch(
                    "http://templ-api.webase.uz/Bank/Update",
                    {
                      method: "POST",
                      headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        Authorization: "Bearer " + tokenStr,
                      },
                      body: JSON.stringify(dataedit),
                    }
                  );
                  let result = await response.json();
                  console.log(result);
                })();

                setData(updatedRows);
                resolve();
              }, 2000);
            }),
        }}
      />

      <div style={{ backgroundColor: "white" }}>
        <TablePagination
          component="div"
          count={Math.trunc(res.length)}
          page={page}
          onPageChange={handleChangePage}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </div>
    </div>
  );
}

export default App;
