import React from "react";
import { Redirect } from "react-router";
import HomeRoute from "../features/home/index.js";
import Content from "../components/content/content.jsx";
import Login from "../features/account/Login.js";
import Logout from "../features/account/Logout.js";
import App from "../features/table/Banktable.js";
import Bank from "../features/table/Contractor.js";
let token = localStorage.getItem("token");
console.log("token", token);
const routes = [
  {
    path: "/",
    exact: true,

    component: () =>
      token ? <Redirect to="/home" /> : <Redirect to="/login" />,
  },
  {
    path: "/home",
    exact: true,
    title: "Home page",
    component: () => (token ? <HomeRoute /> : <Redirect to="/login" />),
  },

  {
    path: "/login",
    title: "Login",
    component: () => <Login />,
  },
  {
    path: "/logout",
    title: "Logout",
    component: () => <Logout />,
  },

  {
    path: "/tableui",
    title: "TableUi",
    component: () => (token ? <App /> : <Redirect to="/login" />),
  },
  {
    path: "/bank",
    title: "Bank",
    component: () => (token ? <Bank /> : <Redirect to="/login" />),
  },
];

export default routes;
