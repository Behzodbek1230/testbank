import React from "react";
import { Route, withRouter } from "react-router-dom";
import RegionAndDistrict from "../../features/regions/RegionAndDistrict.js";
import routes from "./../../config/page-route.jsx";
import { PageSettings } from "./../../config/page-settings.js";

function setTitle(path, routeArray) {
  var pageTitle;
  for (var i = 0; i < routeArray.length; i++) {
    if (routeArray[i].path === path) {
      pageTitle = "Test | " + routeArray[i].title;
    }
  }
  document.title = pageTitle ? pageTitle : "Test";
}

class Content extends React.Component {
  componentDidMount() {
    setTitle(this.props.history.location.pathname, routes);
  }
  componentWillMount() {
    this.props.history.listen(() => {
      setTitle(this.props.history.location.pathname, routes);
    });
  }

  render() {
    return (
      <PageSettings.Consumer>
        {({
          pageContentFullWidth,
          pageContentClass,
          pageContentInverseMode,
        }) => (
          <div
            className={
              "content " +
              (pageContentFullWidth ? "content-full-width " : "") +
              (pageContentInverseMode ? "content-inverse-mode " : "") +
              pageContentClass
            }
          >
            <Route
              path="/regions-and-districts"
              exact="true"
              component={RegionAndDistrict}
            />
          </div>
        )}
      </PageSettings.Consumer>
    );
  }
}

export default withRouter(Content);
