import React from "react";
import { Link } from "react-router-dom";
import { PageSettings } from "./../../config/page-settings.js";

class SidebarProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profileActive: 0,
    };
    this.handleProfileExpand = this.handleProfileExpand.bind(this);
  }

  handleProfileExpand(e) {
    e.preventDefault();
    this.setState((state) => ({
      profileActive: !this.state.profileActive,
    }));
  }

  render() {
    return (
      <PageSettings.Consumer>
        {({ pageSidebarMinify }) => <ul className="nav"></ul>}
      </PageSettings.Consumer>
    );
  }
}

export default SidebarProfile;
