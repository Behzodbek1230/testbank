import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import routes from "./config/page-route.jsx";

export default function App() {
  return (
    <Router>
      <Switch>
        {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Switch>
    </Router>
  );
}
