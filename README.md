## Vis Registon Frontend

# I - Installation on the server

### install yarn v =>10

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

sudo apt-get install -y nodejs
```

### Setup project

```
yarn install
```

### Start project

```
yarn start
```

# II - Installation on local

### Requirements

```
node >=12.0.0
yarn ^1.0.0
```

### Setup project

```
yarn install
```

### Start project

```
yarn start
```
